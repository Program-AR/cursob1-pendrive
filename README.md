# cursoB1-Pendrive

El pendrive recomendado para bajarse para dar el curso B1.

Actualmente alojado y actualizado en este repositorio: `https://gitlab.com/Program-AR/cursob1-pendrive`

Con descargarse el contenido de este repo ya se puede usar (arriba, el ícono de descarga).